/*
 * Copyright 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_TAG "lights"

#include <cutils/log.h>

#include <malloc.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>

#include <sys/ioctl.h>
#include <sys/types.h>

#include <hardware/lights.h>
#include <hardware/hardware.h>

static pthread_once_t g_init = PTHREAD_ONCE_INIT;
static pthread_mutex_t g_lock = PTHREAD_MUTEX_INITIALIZER;

typedef struct {
  struct light_device_t light;
  int led;
  int already_warned;
} led_device_t;

void init_g_lock(void) { pthread_mutex_init(&g_lock, NULL); }

static int write_int(led_device_t *leddev, char const *pathfmt, int value) {
  int err = 0;
  int fd;
  char path[PATH_MAX];

  snprintf(path, sizeof(path), pathfmt, leddev->led);
  fd = open(path, O_WRONLY);
  if (fd < 0) {
    err = -errno;
    if (leddev->already_warned == 0) {
      ALOGE("write_int failed to open %s\n", path);
      leddev->already_warned = 1;
    }
  } else {
    char buffer[20];
    int bytes = snprintf(buffer, sizeof(buffer), "%d\n", value);
    if (bytes < 0) {
      err = -errno;
    } else {
      int amt = write(fd, buffer, bytes);
      if (amt == -1) err = -errno;
    }
    close(fd);
  }
  return err;
}

static int set_led(struct light_device_t *dev,
                   struct light_state_t const *state) {
  led_device_t *leddev = (led_device_t *)dev;
  int err;

  pthread_mutex_lock(&g_lock);
  ALOGV("set_led_notifications, flashMode:%x, color:%x", state->flashMode,
        state->color);
  if (state->flashMode) {
    err = write_int(leddev, "/sys/class/leds/marduk:red:user%d/delay_off",
                    state->flashOffMS);
    if (err == 0)
      err = write_int(leddev, "/sys/class/leds/marduk:red:user%d/delay_on",
                      state->flashOnMS);
  } else {
    /* The driver regards color as 0(off)/anything-else(on). */
    err = write_int(leddev, "/sys/class/leds/marduk:red:user%d/brightness",
                    state->color);
  }
  pthread_mutex_unlock(&g_lock);
  return err;
}

static int close_lights(struct light_device_t *dev) {
  if (dev) free(dev);
  return 0;
}

/* Open a new instance of a lights device using name. */
static int open_lights(const struct hw_module_t *module, char const *name,
                       struct hw_device_t **device) {
  led_device_t *leddev;
  int led;

  ALOGV("open lights");

  if (0 == strcmp(LIGHT_ID_KEYBOARD, name))
    led = 1;
  else if (0 == strcmp(LIGHT_ID_BUTTONS, name))
    led = 2;
  else if (0 == strcmp(LIGHT_ID_BATTERY, name))
    led = 3;
  else if (0 == strcmp(LIGHT_ID_NOTIFICATIONS, name))
    led = 4;
  else if (0 == strcmp(LIGHT_ID_ATTENTION, name))
    led = 5;
  else if (0 == strcmp(LIGHT_ID_BLUETOOTH, name))
    led = 6;
  else if (0 == strcmp(LIGHT_ID_WIFI, name))
    led = 7;
  else
    return -EINVAL;

  leddev = malloc(sizeof(*leddev));
  if (leddev == NULL) {
    ALOGE("failed to allocate memory");
    return -ENOMEM;
  }
  memset(leddev, 0, sizeof(*leddev));

  pthread_once(&g_init, init_g_lock);

  leddev->light.common.tag = HARDWARE_DEVICE_TAG;
  leddev->light.common.version = 0;
  leddev->light.common.module = (struct hw_module_t *)module;
  leddev->light.common.close = (int (*)(struct hw_device_t *))close_lights;
  leddev->light.set_light = set_led;
  leddev->led = led;

  *device = (struct hw_device_t *)leddev;

  return 0;
}

static struct hw_module_methods_t lights_methods = {
    .open = open_lights,
};

struct hw_module_t HAL_MODULE_INFO_SYM = {
    .tag = HARDWARE_MODULE_TAG,
    .version_major = 1,
    .version_minor = 0,
    .id = LIGHTS_HARDWARE_MODULE_ID,
    .name = "Creator lights Module",
    .author = "Imagination",
    .methods = &lights_methods,
};
