#
# Copyright 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

LOCAL_PATH := hardware/bsp/imagination/peripheral/wifi/uccp420

# UCCP420 WIFI Firmware
UCCP420_WIFI_FW_SRC = vendor/bsp/imagination/peripheral/wifi/uccp420_firmware
UCCP420_WIFI_FW_DST = system/vendor/firmware/img/uccp420wlan

PRODUCT_COPY_FILES += \
    $(UCCP420_WIFI_FW_SRC)/v4_5_8/MAC_LOADER.ldr:$(UCCP420_WIFI_FW_DST)/MAC_LOADER.ldr \
    $(UCCP420_WIFI_FW_SRC)/v4_5_8/MCP_LOADER.ldr:$(UCCP420_WIFI_FW_DST)/MCP_LOADER.ldr

BOARD_SEPOLICY_DIRS += \
    $(LOCAL_PATH)/sepolicy

DEVICE_PACKAGES += \
    wifisetmac

WIFI_DRIVER_HAL_MODULE := wifi_driver.$(soc_name)
WIFI_DRIVER_HAL_PERIPHERAL := uccp420
